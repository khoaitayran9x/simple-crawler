<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Crawler\Crawler;
class ClawerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:run {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Crawler';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Crawler::create()
        ->setCrawlObserver(new \App\Core\Crawler())
        ->setConcurrency(1) 
        ->setMaximumCrawlCount(10)
        ->setDelayBetweenRequests(100)
        ->ignoreRobots()
        ->startCrawling($this->argument('url'));
    }
}
