<?php 

namespace App\Core;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlObserver;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;
class Crawler extends CrawlObserver{

	private $pages = [];

	public function willCrawl(UriInterface $url)
    {
    	echo "Now crawling: " . $url . PHP_EOL;
    }


    public function crawled(
        UriInterface $url,
        ResponseInterface $response,
        ?UriInterface $foundOnUrl = null
    )
    {

        $crawler = new DomCrawler((string)$response->getBody());
        $title = $crawler->filterXPath('//title[1]');
        if($title->count() > 0){
	        $this->pages[] =  $url;
	        echo "====". $title->text() . PHP_EOL;
        }
       
    }

    public function crawlFailed(
        UriInterface $url,
        RequestException $requestException,
        ?UriInterface $foundOnUrl = null
    )
    {
        echo 'Clawer Failed!!';
    }

    public function finishedCrawling()
    {
        echo 'Crawled total ' . count($this->pages) . ' urls' . PHP_EOL;
    }
}
